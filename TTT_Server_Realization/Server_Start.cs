﻿using System;
using System.Threading;
using TTT_Server;
using TTT_Server.Controller;


namespace TTT_Server_Realization
{
    class Server_Start
    {
        static void Main(string[] args)
        {

             PressAnyKeyToStart();
        }
        static void PressAnyKeyToStart()
        {
            Game game = new Game();
            Connecter connecter = new Connecter(game);
            Thread connecterThread = new Thread(new ThreadStart(connecter.Connecting));
            Console.WriteLine("Начало работы реализации сервера");
            connecterThread.Start();
            Console.WriteLine("Поток запущен. " + connecterThread.IsAlive.ToString());
            Thread getPlayerThreat = new Thread(new ThreadStart(connecter.GetPlayer));
            getPlayerThreat.Start();
            Console.WriteLine("Ожидаем подключение игроков");
            Thread threadForFieldView = new Thread(new ThreadStart(connecter.GetFieldToView));
            Console.WriteLine("Начало работы реализации сервера");
            threadForFieldView.Start();
        }
    }
}
