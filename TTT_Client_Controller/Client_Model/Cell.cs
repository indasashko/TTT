﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTT_Client_Controller.Client_Model
{
    class Cell
    {
        public int Position { get; set; }
        public Images Image { get; set; }
        public Cell(int position, Images image)
        {
            Position = position;
            Image = image;
        }
    }
}
