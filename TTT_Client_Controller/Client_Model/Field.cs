﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTT_Client_Controller.Client_Model
{
     public class Field
    {
        public int ThisPlayer;
        Cell[] cells = new Cell[9];
        int CurrentTurn = 1;
        int[] turn = new int[2];
        int[] ParseData(string data)
        {
            return new int[] { int.Parse(data[0].ToString()), int.Parse(data[2].ToString()) };
        }
        void ChangeCurrentTurn()
        {
            if (CurrentTurn == 1)
            {
                CurrentTurn++;
            }
            if (CurrentTurn == 2)
            {
                CurrentTurn--;
            }
        }
        public void ChangeCell(string data)
        {
            int check;
            if (!int.TryParse(data[0].ToString(), out check))
            {
                Console.WriteLine("Данные не соответсвуют формату хода");
            }
            Console.WriteLine("Мутации на стороне вью");
            int firstSymbol = (int.Parse(data[0].ToString()));
            if (CurrentTurn == firstSymbol)
            {
                Console.WriteLine("Мутации начаты");
                int[] turn = ParseData(data);
                Console.WriteLine($"Сервер ответил {turn[0]}{turn[1]}");
                cells[turn[1]].Image = turn[0] == 1 ? Images.Krest : Images.Nole;
                ChangeCurrentTurn();
            }
        }
        public Field()
        {
            CurrentTurn = 1;
            for (int i = 0; i < cells.Length; i++)
            {
                cells[i] = new Cell(i + 1, Images.None);
            }
        }
    }
}
