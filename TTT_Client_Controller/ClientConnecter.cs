﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using TTT_Client_Controller.Client_Model;

namespace TTT_Client_Controller
{
    public class ClientConnecter
    {
        public Field gameField;
        public ClientConnecter()
        {
            gameField = new Field();
        }
        public void ConnectToServer(string dataToSend)
        {
            const string ip = "127.0.0.1";
            const int port = 8080;
            IPEndPoint tcpEndPoint = new IPEndPoint(IPAddress.Parse(ip), port);
            Socket tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            if (!DataCheck(dataToSend))
            {
                Console.WriteLine("Данные не корректны");
                return;
            }
            //dataToSend = this.gameField.ThisPlayer.ToString() + "/" + dataToSend;
            var data = Encoding.UTF8.GetBytes(dataToSend);
            tcpSocket.Connect(tcpEndPoint);
            tcpSocket.Send(data);
            byte[] buffer = new byte[256];
            int size = 0;
            StringBuilder answer = new StringBuilder();
            do
            {
                size = tcpSocket.Receive(buffer);
                answer.Append(Encoding.UTF8.GetString(buffer, 0, size));
            }
            while (tcpSocket.Available > 0);
            Console.WriteLine("Сервер ответил -"+answer.ToString());
            gameField.ChangeCell(answer.ToString());
            tcpSocket.Shutdown(SocketShutdown.Both);
            tcpSocket.Close();
        }

        public void GetPlayer(string dataToSend)
        {
            const string ip = "127.0.0.1";
            const int port = 8081;
            IPEndPoint tcpEndPoint = new IPEndPoint(IPAddress.Parse(ip), port);
            Socket tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            var data = Encoding.UTF8.GetBytes(dataToSend);
            tcpSocket.Connect(tcpEndPoint);
            if (!DataCheck(dataToSend))
            {
                Console.WriteLine("Данные не корректны");
                return;
            }
            tcpSocket.Send(data);
            byte[] buffer = new byte[256];
            int size = 0;
            StringBuilder answer = new StringBuilder();
            do
            {
                size = tcpSocket.Receive(buffer);
                answer.Append(Encoding.UTF8.GetString(buffer, 0, size));
            }
            while (tcpSocket.Available > 0);
            // и  че мы делаем с этой инфо?
            int playerNumber;
            if (int.TryParse(answer.ToString(), out playerNumber))
            {
                this.gameField.ThisPlayer = playerNumber;
            }
            else
            {
                Console.WriteLine("Не корректные данные для получения номера игрока");
            }
            Console.WriteLine(answer.ToString());
            tcpSocket.Shutdown(SocketShutdown.Both);
            tcpSocket.Close();
        }

        public string GetFieldFromServer(string dataToSend)
        {
            const string ip = "127.0.0.1";
            const int port = 8082;
            IPEndPoint tcpEndPoint = new IPEndPoint(IPAddress.Parse(ip), port);
            Socket tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            var data = Encoding.UTF8.GetBytes(dataToSend);
            tcpSocket.Connect(tcpEndPoint);
            tcpSocket.Send(data);
            byte[] buffer = new byte[256];
            int size = 0;
            StringBuilder answer = new StringBuilder();
            do
            {
                size = tcpSocket.Receive(buffer);
                answer.Append(Encoding.UTF8.GetString(buffer, 0, size));
            }
            while (tcpSocket.Available > 0);
            tcpSocket.Shutdown(SocketShutdown.Both);
            tcpSocket.Close();
            return answer.ToString();
        }
        public bool DataCheck(string response)
        {
            if(response == "start")
            return true;
            string[] splitResponse = response.Split('/');
            if (splitResponse.Length == 2)
            {
                int cheker;
                if(int.TryParse(splitResponse[0],out cheker) && int.TryParse(splitResponse[1], out cheker))
                {
                    return true;
                }
                Console.WriteLine($"Возникли проблеммы с данными {response}. парсинг в инт");
                return false;
            }
            Console.WriteLine($"Возникли проблеммы с данными {response}. Ошибка length");
            return false;
        }
        public Field GetGameField ()
        {
            return this.gameField;
        }
       
    }
}
