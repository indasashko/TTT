﻿using System;
using TTT_Client_Controller;
using TTT_Console_Client.View;

namespace TTT_Console_Client
{
    class Program
    {
        static void Main(string[] args)
        {

            StartGame();
            
        }
        static void StartGame()
        {
            ClientConnecter clientConnecter = new ClientConnecter();
            clientConnecter.GetPlayer("start");
            while (true)
            {
                string message = $"{clientConnecter.gameField.ThisPlayer.ToString()}/" + Console.ReadLine();
                if (message == "1/quit" || message == "2/quit")
                    break;
                clientConnecter.ConnectToServer(message);
                Console.ReadKey();
                string ViewField = clientConnecter.GetFieldFromServer(" ");
                FieldViewConsole.WriteFieldToConsole(ViewField);
            }
        }

    }
}
