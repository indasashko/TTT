﻿using System;
using System.Collections.Generic;
using System.Text;
using TTT_Client_Controller.Client_Model;

namespace TTT_Console_Client.View
{
    public static class FieldViewConsole
    {

        public static void WriteFieldToConsole(string data)
        {

            char[] symbols = new char[9];
            for (int i = 0; i<9;i++)
            {
                symbols[i] = data[i];
            }
            Console.WriteLine($"{symbols[0]}|{symbols[1]}|{symbols[2]}");
            Console.WriteLine("-----");
            Console.WriteLine($"{symbols[3]}|{symbols[4]}|{symbols[5]}");
            Console.WriteLine("-----");
            Console.WriteLine($"{symbols[6]}|{symbols[7]}|{symbols[8]}");
        }

    }
}
